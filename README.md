# First Deployment Leanderson Coelho

## Rodar 

Faça o `build` da aplicação

> mvn clean package

Exponha a porta configurada

> export WSPORT=8983

Cria a imagem docker da aplicação 

> docker build -t firstdeployment .

Inicie o container

> docker run --rm -p $WSPORT:8080 firstdeployment

Ou em background:

> docker run -d --rm -p $WSPORT:8080 firstdeployment

### Teste

Faça uma requisição para `/status`

Caso o comando `docker run` tenha sido em background:
*aguarde alguns segundos para aplicação inicar 1~3s*
> curl http://localhost:$WSPORT/status

Se não, em outra aba

> curl http://localhost:8983/status